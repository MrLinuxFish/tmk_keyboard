#include "extended_keymap_common.h"

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
[0] = { /* QWERTY (Default) */
  {KC_TAB,    KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_BSPC},
  {KC_ESC,    KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT},
  {KC_LSFT,   KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, FUNC(9)},
  {KC_LCTL,   FUNC(4), KC_LGUI, KC_LALT, FUNC(3), KC_SPC,  KC_NO,   FUNC(2), KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT}
},
[1] = { /* Gaming */
  {KC_TAB,  KC_TRNS, KC_Q,    KC_W,    KC_E,    KC_R,    KC_J,    KC_L,    KC_U,    KC_UP,   KC_SCLN, KC_BSPC},
  {KC_ESC,  KC_TRNS, KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_LEFT, KC_DOWN, KC_RGHT, KC_QUOT},
  {KC_LSFT, KC_LCTL, KC_Z,    KC_X,    KC_C,    KC_V,    KC_K,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, FUNC(9)},
  {KC_LCTL, FUNC(5), KC_LGUI, KC_LALT, FUNC(3), KC_SPC,  KC_NO,   FUNC(2), KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT}
},
[2] = { /* RAISE */
  {KC_GRV,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,     KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_TRNS},
  {KC_EJCT, KC_APP,  KC_TRNS, KC_TRNS, KC_DEL,  KC_SLCK,  KC_NLCK, KC_MINS, KC_EQL,  KC_LBRC, KC_RBRC, KC_BSLS},
  {KC_CAPS, KC_F11,  KC_F12,  KC_F13,  KC_F14,  KC_F15,   KC_F16,  KC_F17,  KC_F18,  KC_F19,  KC_F20,  KC_TRNS},
  {KC_RCTL, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,  KC_NO,   FUNC(2), KC_MPRV, KC_VOLD, KC_VOLU, KC_MNXT}
},
[3] = { /* LOWER */
  {S(KC_GRV),  S(KC_1), S(KC_2), S(KC_3), S(KC_4), S(KC_5), S(KC_6), S(KC_7),    S(KC_8),   S(KC_9),    S(KC_0),    KC_TRNS},
  {KC_TRNS,    KC_TRNS, KC_TRNS, KC_TRNS, KC_INS,  KC_PSCR, KC_PAUS, S(KC_MINS), S(KC_EQL), S(KC_LBRC), S(KC_RBRC), S(KC_BSLS)},
  {S(KC_ENT),  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,      KC_F8,     KC_F9,      KC_F10,     KC_TRNS},
  {KC_TRNS,    KC_TRNS, KC_TRNS, KC_TRNS, FUNC(3), KC_TRNS, KC_NO,   KC_TRNS,    KC_HOME,   KC_PGDN,    KC_PGUP,    KC_END}
},
[4] = { /* MOUSE KEYS & 10 KEY */
  {KC_TRNS, KC_WH_U, KC_BTN2, KC_MS_U, KC_BTN1, KC_BTN4, KC_PSLS, KC_P7,   KC_P8,   KC_P9,   KC_PMNS, KC_TRNS},
  {KC_TRNS, KC_WH_D, KC_MS_L, KC_MS_D, KC_MS_R, KC_BTN5, KC_PAST, KC_P4,   KC_P5,   KC_P6,   KC_PPLS, KC_PDOT},
  {KC_TRNS, KC_MPRV, KC_MPLY, KC_MNXT, KC_TRNS, KC_NLCK, KC_P0,   KC_P1,   KC_P2,   KC_P3,   KC_PENT, KC_TRNS},
  {KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_P0,   KC_NO,   KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS}
},
/*[5] = { /* SPACE FN (LINUX CMDS)
  {KC_TRNS, C(A(KC_F1)), C(A(KC_F2)), C(A(KC_F3)), C(A(KC_F4)), C(A(KC_F5)), C(A(KC_F6)), C(A(KC_F7)), C(A(KC_F8)), C(A(KC_F9)), C(A(KC_F10)), KC_TRNS},
  {G(KC_W), G(KC_B),     G(KC_S),     G(KC_TAB),   G(KC_F),     G(KC_GRV),   G(KC_H),     G(KC_J),     G(KC_K),     G(KC_L),     G(KC_SCLN),   KC_TRNS},
  {KC_LSFT, G(KC_1),     G(KC_2),     G(KC_3),     G(KC_4),     G(KC_5),     G(KC_6),     G(KC_7),     G(KC_8),     G(KC_9),     G(KC_0),      G(KC_ENT)},
  {KC_LCTL, KC_TRNS,     KC_TRNS,     KC_LALT,     KC_TRNS,     KC_TRNS,     KC_NO,       FUNC(11),    KC_TRNS,     KC_TRNS,     KC_TRNS,      KC_TRNS}
},
[6] = { /* SPACE FN (LINUX CMDS) EMBEDDED FN
  {KC_TRNS, KC_TRNS,     KC_TRNS,     KC_TRNS,     KC_TRNS,     KC_TRNS,     KC_TRNS,     KC_TRNS,     KC_TRNS,     KC_TRNS,     KC_TRNS,    KC_TRNS},
  {KC_TRNS, KC_TRNS,     KC_TRNS,     KC_TRNS,     KC_TRNS,     KC_TRNS,     G(KC_Z),     G(KC_LBRC),  G(KC_RBRC),  G(KC_X),     KC_TRNS,    KC_TRNS},
  {KC_TRNS, G(S(KC_1)),  G(S(KC_2)),  G(S(KC_3)),  G(S(KC_4)),  G(S(KC_5)),  G(S(KC_6)),  G(S(KC_7)),  G(S(KC_8)),  G(S(KC_9)),  G(S(KC_0)), KC_TRNS},
  {KC_TRNS, KC_TRNS,     KC_TRNS,     KC_TRNS,     KC_TRNS,     KC_TRNS,     KC_NO,       KC_TRNS,     KC_TRNS,     KC_TRNS,     KC_TRNS,    KC_TRNS}
}
*/
};

const uint16_t PROGMEM fn_actions[] = {
    [2] = ACTION_LAYER_MOMENTARY(2),  				// RAISE
    [3] = ACTION_LAYER_MOMENTARY(3),  				// LOWER
    [4] = ACTION_LAYER_MOMENTARY(4),				// Momentary mouse keys
    [5] = ACTION_LAYER_OFF_ON(1),  				// Disable Gaming layout on down press, Enable Gaming layout on upstroke
    [8] = ACTION_LAYER_TOGGLE(1),				// set to Gaming Keymap
    [9] = ACTION_MODS_TAP_KEY(MOD_RSFT, KC_ENT),	        // Short press SHIFT, Long press ENTER
    [10] = ACTION_MODS_TAP_KEY(MOD_LCTL, KC_ESC),	        // Short press ESC, Long press CRTL
/*	[11] = ACTION_LAYER_TAP_KEY(5, KC_SPC),			// Space fn (layer 5)
	[12] = ACTION_LAYER_MOMENTARY (6)		       	// Second space fn layer (6)
*/
};
