/*
Copyright 2012 Jun Wako <wakojun@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <avr/io.h>
#include "stdint.h"
#include "led.h"


// Caps Lock led settings (set to B0)
void led_set(uint8_t usb_led)
{
    // Using B7 Caps Lock LED
    if (usb_led & (1<<USB_LED_CAPS_LOCK))
    {
        // Output high.
        DDRB |= (1<<0);
        PORTB |= (1<<0);
    }
    else
    {
        // Output low.
        DDRB &= ~(1<<0);
        PORTB &= ~(1<<0);
    }
}



// Led layer toggle
void led_layer_set(uint32_t state) {
    DDRB |= (1<<7); //led 0 (B7)
    //dprint("Set LEDs for layer change\n");

    // Led for Layer 1
    if ((1<<1 & state) != 0) {
        //dprint("Layer 2: on\n");
        PORTB |= (1<<7);
    } else {
        //dprint("Layer 2: off\n");
        PORTB &= ~(1<<7);
    }
	
    DDRB |= (1<<3); //led 1 (B3)
    //dprint("Set LEDs for layer change\n");

    // Led for Layer 4
    if ((1<<4 & state) != 0) {
        //dprint("Layer 2: on\n");
        PORTB |= (1<<3);
    } else {
        //dprint("Layer 2: off\n");
        PORTB &= ~(1<<3);
    }
	
    DDRB |= (1<<2); //led 2 (B2)
    //dprint("Set LEDs for layer change\n");

    // Led for Layer 2
    if ((1<<2 & state) != 0) {
        //dprint("Layer 2: on\n");
        PORTB |= (1<<2);
    } else {
        //dprint("Layer 2: off\n");
        PORTB &= ~(1<<2);
    }
	
    DDRB |= (1<<1); //led 3 (B1)
    //dprint("Set LEDs for layer change\n");

    // Led for Layer 3
    if ((1<<3 & state) != 0) {
        //dprint("Layer 2: on\n");
        PORTB |= (1<<1);
    } else {
        //dprint("Layer 2: off\n");
        PORTB &= ~(1<<1);
    }
}

